import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.lang.StrictMath.abs;

/**
 * Created by maciej on 20.05.17.
 */
public class Closing {

    private BufferedImage orgImage;
    public BufferedImage getNewImage() {
        return newImage;
    }
    private BufferedImage newImage;
    private Integer angle,length;
    private boolean[][] structuralElement;
    private int HEH = 0, HEW = 0;// HEH - half (structural ) element height, HEW - - half (structural) element width
    private File imageFile;
    appWin parent;

    Closing(Integer angle, Integer length, appWin parent){
        if( angle == null || length == null)
            throw new IllegalArgumentException("Angle and/or length are null!");

        if( angle >= 360){
            do{
                angle-=180;
            }while(angle >= 180);
        }
        this.parent = parent;
        this.angle = angle;
        this.length = length;

        try {
            imageFile = parent.showFileChooser();
            orgImage = ImageIO.read(imageFile);
            newImage = ImageIO.read(imageFile);//TODO: sprawdzic czy działa
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        createStructuralElement();

    }
    private void createStructuralElement(){
        int height = (int)Math.ceil( abs(Math.sin(angle*Math.PI/180)) * length );
        int width =  (int)Math.ceil( abs(Math.cos(angle*Math.PI/180)) * length);

        HEH = height/2;
        HEW = width/2;

        if(height==0)++height;
        if(width==0)++width;

        System.out.println(height+" "+width);
        structuralElement = new boolean[height][width];//[][length]?

        if( angle >= 90 && angle < 180){
            //rysuj od gornego lewego rogu
            double i = 0d,j = 0d;
            if( angle < 135 ){
                for(; i<height && j < width ;i+=1, j+=1.0*width/height){
                    int coordinateI = (int)Math.floor(i);
                    int coordinateJ = (int)Math.floor(j);
                    structuralElement[coordinateI][coordinateJ] = true;
                }
            }else{
                for(; j<width && i<height  ; j+=1, i+=1.0*height/width){
                    int coordinateI = (int)Math.floor(i);
                    int coordinateJ = (int)Math.floor(j);
                    structuralElement[coordinateI][coordinateJ] = true;
                }
            }
        }


        if( angle >= 0 && angle < 90){
            if( angle > 45 ){
                double j=0,i=height-1;
                for(;i>=0 && Double.compare(j,width)<0;i-=1, j+=1.0*width/height){
                    int coordinateI = (int)i;
                    int coordinateJ = (int)j;
                    structuralElement[coordinateI][coordinateJ] = true;
                }
            }

            double i=height-1.0*height/width,
                   j=0d;
            for(;i>=-1 && Double.compare(j,width)<0;j+=1, i-=1.0*height/width){
                int coordinateI = (int)i;
                int coordinateJ = (int)j;
                structuralElement[coordinateI][coordinateJ] = true;
            }

        }

        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++) {
                System.out.print(structuralElement[i][j]+" ");
            }
            System.out.println();
        }
        int rows = structuralElement.length;
        int columns = structuralElement[0].length;

        System.out.println("Rows : " + rows + ", columns : " + columns);

    }
    public void close(){
        ditale();
        parent.mainPanel.saveImage(newImage,"after_dilatation");
        File tmpFile = new File("after_dilatation.png");
        try {
            orgImage = ImageIO.read(tmpFile);
            newImage = ImageIO.read(tmpFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        erode();

        //tmpFile.delete();
    }
    private void ditale(){
        System.out.println(HEH + ", " + HEW);

        int trueColor = new Color(255,255,255).getRGB();

        int imageHeight = newImage.getHeight();
        int imageWidth = newImage.getWidth();

        for( int i = HEH; i < imageHeight-HEH;i++ ){
            for( int j = HEW;j < imageWidth-HEW; j++){
                if ( checkMaskForDilate(j,i) ){
                    newImage.setRGB(j,i,trueColor);
                }
            }
        }
    }
    public boolean checkMaskForDilate(int x, int y){
        double rows = structuralElement.length;
        double columns = structuralElement[0].length;

        int trueColor = new Color(255,255,255).getRGB();

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns;j++){
                if ( structuralElement[i][j] == true && orgImage.getRGB(x-HEW+j,y-HEH+i) == trueColor )
                    return true;
            }
        }
        return false;
    }
    private void erode(){
        int falseColor = new Color(0,0,0).getRGB();

        int imageHeight = newImage.getHeight();
        int imageWidth = newImage.getWidth();

        for( int i = HEH; i < imageHeight-HEH;i++ ){
            for( int j = HEW;j < imageWidth-HEW; j++){
                if ( checkMaskForErode(j,i) ){
                    newImage.setRGB(j,i,falseColor);
                }
            }
        }
    }
    public boolean checkMaskForErode(int x, int y){
        double rows = structuralElement.length;
        double columns = structuralElement[0].length;

        int falseColor = new Color(0,0,0).getRGB();

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns;j++){
                if ( structuralElement[i][j] == true && orgImage.getRGB(x-HEW+j,y-HEH+i) == falseColor )
                    return true;
            }
        }
        return false;
    }

}
