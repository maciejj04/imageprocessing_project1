import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;

/**
 * Created by maciej on 13.05.17.
 */
public class HistogramEqualization {
    Data data = new Data();
    BufferedImage image;

    HistogramEqualization(appWin parent){
        try {
            File imageFile = parent.showFileChooser();
            image = ImageIO.read(imageFile);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
    public void runEqualization(int L){
        for( int i = 0; i < image.getWidth(); i++ ) {
            for (int j = 0; j < image.getHeight(); j++) {
                int[] rgb = getPixelData(i, j);
                data.R[rgb[0]]++;
                data.B[rgb[1]]++;
                data.G[rgb[2]]++;
            }
        }

        for ( int i = 1 ;i < 256; i++){
            data.R[i]+=data.R[i-1];
            data.G[i]+=data.G[i-1];
            data.B[i]+=data.B[i-1];
        }

        int minR = Arrays.stream(data.R).filter( e -> e != 0 ).min().getAsInt();
        int minG = Arrays.stream(data.G).filter( e -> e != 0 ).min().getAsInt();
        int minB = Arrays.stream(data.B).filter( e -> e != 0 ).min().getAsInt();
        System.out.println("MIN: "+minR);

        int height = image.getHeight();
        int width = image.getWidth();


        for( int i = 0; i < width; i++ ) {
            for (int j = 0; j < height; j++) {
                int[] color = getPixelData(i,j);
                int newR = (int) (( 1.0*(data.R[color[0]] - minR)/
                                        ( width*height - minR )     )* (L-1));
                int newG = (int) (( 1.0*(data.G[color[1]] - minG)/
                                        ( width*height - minR )     )* (L-1));
                int newB = (int) (( 1.0*(data.B[color[2]] - minB)/
                                        ( width*height - minR )     )* (L-1));

                image.setRGB(i,j, new Color(newR,newG,newB).getRGB() );
            }
        }


    }
    private int[] getPixelData( int x, int y ) {
        int argb = image.getRGB(x, y);

        int rgb[] = new int[] {
                (argb >> 16) & 0xff, //red
                (argb >>  8) & 0xff, //green
                (argb      ) & 0xff  //blue
        };
        Color color = new Color(argb);
        //System.out.println("rgb: " + rgb[0] + " " + rgb[1] + " " + rgb[2]);
        return rgb;
    }

}
