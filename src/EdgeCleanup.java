import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by maciej on 21.05.17.
 */
public class EdgeCleanup {

    BufferedImage orgImage;
    BufferedImage newImage;
    BufferedImage marker=null;
    mainPanel parent;
    BufferedImage dilated=null;
    File imageFile;

    EdgeCleanup(mainPanel parent){
        this.parent = parent;
        try {
            imageFile = parent.parent.showFileChooser();
            orgImage = ImageIO.read(imageFile);
            newImage = ImageIO.read(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        orgImage = parent.parent.image;
//        newImage = new BufferedImage(orgImage.getWidth(),orgImage.getHeight(),orgImage.getType());

        createMarker();
        parent.saveImage(marker,"marker");


//        this.dilated = parent.copyBufferedImage(marker);
//        /*
//        *   Czytam marker jeszcze raz ponieważ, nie wiadomo czego bez tego delate.setRGB(...)
//        *   zmieniało dilated ORAZ marker
//        *
//        * */
//        try {
//            marker = ImageIO.read(new File("/home/maciej/IdeaProjects/ImageProcessing_project1/marker.png"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        dilate();
//        try {
//            orgImage = ImageIO.read(parent.parent.imageFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        AND();
//        dilate();
        reconstruct();
        parent.saveImage(newImage,"edgeElements");
    }

    private void createMarker() {

        marker = parent.copyBufferedImage(orgImage);

        int black = new Color(0,0,0).getRGB();
        for(int i = 1; i < marker.getHeight()-1;i++){
            for ( int  j = 1; j < marker.getWidth()-1;j++){
                marker.setRGB(j,i,black);
            }
        }
    }
    private void dilate(){

        int width = dilated.getWidth();
        int height = dilated.getHeight();

        for(int i = 1;i < height-1;i++){
            for(int j = 1; j < width-1;j++){
                if ( checkPixelSorroundings(j,i) ) {
                    dilated.setRGB(j, i, new Color(255, 255, 255).getRGB());
                }
            }
        }
    }

    private boolean checkPixelSorroundings(int x,int y){

        int one = new Color(255,255,255).getRGB();

        for( int j = y-1;j<=y+1;j++){
            for (int i = x-1;i<=x+1;i++){
                try {
                    if (marker.getRGB(i,j) == one)
                        return true;
                }catch (Exception ex){
                    System.out.println(ex.getMessage());
                    System.out.println(i + " " + j+"\n x,y="+x+","+y);
                    return true;
                }
            }
        }
        return false;
    }

    private void AND(){
        int white = new Color(255,255,255).getRGB();
        int black = new Color(0,0,0).getRGB();

        try {orgImage = ImageIO.read(parent.parent.imageFile);} catch (IOException e) {e.printStackTrace();   }

        for(int i = 0;i < orgImage.getHeight();i++){
            for ( int j = 0; j < orgImage.getWidth();j++){
                if(  orgImage.getRGB(j,i) == white && dilated.getRGB(j,i) == white  ){
                    newImage.setRGB(j,i,white);
                }
                else{
                    newImage.setRGB(j,i,black);
                }
            }
        }

    }
    private boolean areDiffrent(int version){

        BufferedImage oldVersion=null;
        try {
            oldVersion = ImageIO.read(new File("/home/maciej/IdeaProjects/ImageProcessing_project1/newImage"+version+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int i = 0;i < orgImage.getHeight();i++) {
            for (int j = 0; j < orgImage.getWidth(); j++) {
                if ( oldVersion.getRGB(j,i) != newImage.getRGB(j,i) )
                    return true;
            }
        }
        return false;
    }

    private void reconstruct(){
        int i=0;

        this.dilated = parent.copyBufferedImage(marker);
        try {marker = ImageIO.read(new File("/home/maciej/IdeaProjects/ImageProcessing_project1/marker.png"));} catch (IOException e) {e.printStackTrace();  }

        dilate();

        AND();
        parent.saveImage(newImage,"newImage0");
        i++;

        while(true){

            this.dilated = parent.copyBufferedImage(newImage);

            try {marker = ImageIO.read(new File("/home/maciej/IdeaProjects/ImageProcessing_project1/newImage"+(i-1)+".png"));} catch (IOException e) {e.printStackTrace();  }

            dilate();

            AND();

            parent.saveImage(newImage,"newImage"+i);

            if( !areDiffrent(i-1) ){
                deleteUnnecessaryFiles(i);
                break;
            }
            i++;
    }
    }


    public void eraseEdgeElements(){
        SUB(orgImage,newImage);
        parent.saveImage(orgImage,"edge_cleanup_result");
    }
    private void SUB(BufferedImage i1, BufferedImage i2){

        int white = new Color(255,255,255).getRGB();
        int black = new Color(0,0,0).getRGB();

        for( int i = 0;i < i1.getHeight();i++){
            for(int j = 0; j< i2.getWidth();j++){
                if(i2.getRGB(j,i) == white )
                    i1.setRGB(j,i,black);
            }
        }
    }


    private void deleteUnnecessaryFiles(int i) {
        for(int j = 0;j<=i;j++){
            File file = new File("./newImage"+j+".png");
            file.delete();
        }
    }
}
