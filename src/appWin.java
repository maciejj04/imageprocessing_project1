import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by maciej on 13.05.17.
 */
public class appWin extends JFrame {

    mainPanel       mainPanel = new mainPanel(this);
    JFileChooser    fileChooser = new JFileChooser();
    BufferedImage   image;
    File            imageFile;


    appWin() throws FileNotFoundException {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(450,200));


        add(mainPanel);
        //showFileChooser();
        setVisible(true);
    }

    public File showFileChooser() throws FileNotFoundException {
        fileChooser.setCurrentDirectory(new File("/home/maciej/IdeaProjects/ImageProcessing_project1/"));
        int ret = fileChooser.showOpenDialog(this);

        if (ret == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: " +
                    fileChooser.getSelectedFile().getName());

                imageFile = fileChooser.getSelectedFile();
            try {
                image = ImageIO.read(imageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("mam nadzieje ze podales obrazek nie jakies gowno");

        return imageFile;


        //this.fStream = new DataInputStream(new FileInputStream(file));
    }
}
