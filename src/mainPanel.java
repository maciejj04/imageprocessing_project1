import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

/**
 * Created by maciej on 13.05.17.
 */
public class mainPanel extends JPanel {

    JButton histogramEqualization = new JButton("histogramEqualization");
    JButton gaussFilter = new JButton("gaussFilter");
    JButton closing = new JButton("closing");
    JButton edgeCleanup = new JButton("edgeCleanup");

    appWin parent;
    JTextField L = new JTextField("L");
    JTextField maskSize = new JTextField("maskSize");
    JTextField deviation = new JTextField("deviation");


    JTextField angle = new JTextField("angle");
    JTextField length = new JTextField("length");


    mainPanel(appWin parent){
        this.parent = parent;
        setLayout(null);

        histogramEqualization.setBounds(10,10,200,30);
        L.setBounds(220,10,40,30);
        histogramEqualization.addActionListener((ae)->histogramEqualzationAction());




        gaussFilter.setBounds(10,50,200,30);
        maskSize.setBounds(220,50,100,30);
        deviation.setBounds(330,50,100,30);
        gaussFilter.addActionListener((ae)->gaussFIlterAction());
        closing.setBounds(10,90,200,30);
        closing.addActionListener(ae->closingAction());
        angle.setBounds(220,90,100,30);
        length.setBounds(330,90,100,30);


        edgeCleanup.addActionListener(ae->edgeCleanupAction());
        edgeCleanup.setBounds(10,130,200,30);


//        L.setPreferredSize(new Dimension(40,20));


        add(histogramEqualization);
        add(L);
        add(gaussFilter);
        add(maskSize);
        add(deviation);
        add(closing);
        add(angle);
        add(length);
        add(edgeCleanup);

    }

    private void histogramEqualzationAction() {

        //TODO:implement
        String sL = L.getText();
        if ( sL.equals("") )
            throw new IllegalArgumentException();

        int l = Integer.valueOf(sL);

        HistogramEqualization histogramEqualization = new HistogramEqualization(parent);
        histogramEqualization.runEqualization(l);


        saveImage(histogramEqualization.image,"histogramEqualization");
    }

    private void gaussFIlterAction() {

            String ms = maskSize.getText();
            String dev = deviation.getText();

            if ( ms.equals("") || dev.equals(""))
                throw new IllegalArgumentException();

            int MS = Integer.valueOf(ms);
            Double DEV = Double.valueOf(dev);

            GaussAverageMask gaussAverageMask = new GaussAverageMask(parent,MS,DEV);
            gaussAverageMask.filter();

            saveImage(gaussAverageMask.newImage,"gaussFilter");

    }

    private void edgeCleanupAction(){
        EdgeCleanup edgeCleanup = new EdgeCleanup(this);
        edgeCleanup.eraseEdgeElements();
    }
    private void closingAction() {
        Closing closing = new Closing(Integer.valueOf(angle.getText()),Integer.valueOf(length.getText()),
                parent);
        closing.close();
        saveImage(closing.getNewImage(),"closed");
    }

    public void saveImage(BufferedImage image, String name){
        try {
            File outputfile = new File(name+".png");
            ImageIO.write( image, "png", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Saved as " + name + ".png");
    }
    public BufferedImage copyBufferedImage(BufferedImage image)
    {
        ColorModel cm = image.getColorModel();
        boolean premultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = image.copyData(image.getRaster());
        return new BufferedImage(cm, raster, premultiplied, null);
    }

}
