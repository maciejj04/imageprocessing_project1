import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.lang.Math.exp;

/**
 * Created by maciej on 13.05.17.
 */
public class GaussAverageMask {

    private Double[][] mask;
    private int HMS;// half mask size
    private Double maskSum = 0d;
    BufferedImage orgImage;
    BufferedImage newImage;
    File imageFile;
    appWin parent;

    GaussAverageMask(appWin parentFrame, int size,Double deviation){//rozmiar maski i odchylenie

        if ( size%2 != 1 ) {
            throw new IllegalArgumentException("Mask size has to me odd number!");
        }
        this.parent = parentFrame;
        try {
            imageFile = parent.showFileChooser();
            this.orgImage = ImageIO.read(imageFile);
            this.newImage = ImageIO.read(imageFile);

        } catch (IOException e) {
            e.printStackTrace();
        }

        mask = new Double[size][size];
        HMS = size/2;//half of mask size

        for(int i = 0;i < size;i++) {
            for (int j = 0; j < size; j++) {
                mask[i][j] = exp( -( (i-HMS)*(i-HMS)+(j-HMS)*(j-HMS) )/(2*deviation*deviation) );
                maskSum+=mask[i][j];
            }
        }

    }
    public void filter(){
        // I've skipped filtration for edge pixels

        int width = orgImage.getWidth();
        int height = orgImage.getHeight();

        for( int i = HMS; i < width - HMS; i++){
            for( int j = HMS; j < height - HMS; j++){
//                int[] RGB = getPixelData(i,j);
                Color color = multiplyByMask(i,j);

                newImage.setRGB(i,j, color.getRGB() );

            }
        }


    }
    private int[] getPixelData( int x, int y ) {
        int argb = orgImage.getRGB(x, y);

        int rgb[] = new int[] {
                (argb >> 16) & 0xff, //red
                (argb >>  8) & 0xff, //green
                (argb      ) & 0xff  //blue
        };
        //System.out.println("rgb: " + rgb[0] + " " + rgb[1] + " " + rgb[2]);
        return rgb;
    }
    private Color multiplyByMask(int x,int y){

        Double sumR = 0d,
               sumG = 0d,
               sumB = 0d;

        for(int i = x-HMS,i1 = 0;i<=x+HMS;i++,i1++){
            for(int j = y-HMS,j1 = 0; j <= y+HMS; j++,j1++){
                int[] newRGB = getPixelData(i,j);
                sumR+=mask[i1][j1]*newRGB[0];
                sumG+=mask[i1][j1]*newRGB[1];
                sumB+=mask[i1][j1]*newRGB[2];
            }
        }
        sumR/=maskSum;
        sumG/=maskSum;
        sumB/=maskSum;

        return new Color((int)Math.round(sumR),(int)Math.round(sumG),(int)Math.round(sumB));
    }

}
